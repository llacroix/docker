#!/usr/bin/env python
import argparse
import time
import shlex
import subprocess
import sys
import glob
import pip
import re
import string
import random

import os
from os import path
from os.path import expanduser
import signal
from passlib.context import CryptContext

try:
    # python3
    SIGSEGV = signal.SIGSEGV.value
except AttributeError:
    SIGSEGV = signal.SIGSEGV


try:
    # python3
    from configparser import ConfigParser, NoOptionError
except Exception:
    from ConfigParser import ConfigParser, NoOptionError


try:
    # python3
    quote = shlex.quote
except Exception as exc:
    def quote(s):
        """Return a shell-escaped version of the string *s*."""
        _find_unsafe = re.compile(r'[^\w@%+=:,./-]').search
        if not s:
            return "''"
        if _find_unsafe(s) is None:
            return s

        # use single quotes, and put single quotes into double quotes
        # the string $'b is then quoted as '$'"'"'b'
        return "'" + s.replace("'", "'\"'\"'") + "'"


def pipe(args):
    """
    Call the process with std(in,out,err)
    """
    print("Executing external command %s" % " ".join(args))
    process = subprocess.Popen(
        args,
        stdin=sys.stdin,
        stdout=sys.stdout,
        stderr=sys.stderr,
    )

    process.wait()

    print("External command execution completed with returncode(%s)" % process.returncode)

    if process.returncode == -SIGSEGV:
        print("PIPE call segfaulted")
        print("Failed to execute %s" % args)

    # Force a flush of buffer
    flush_streams()

    return process.returncode


def start():
    """
    Main process running odoo
    """
    #quoted_args = [
    #    quote(arg)
    #    for arg in sys.argv[1:]
    #] or ["true"]
    #username = "odoo"

    # Run the command as odoo while everything is quoted
    #return pipe(["su", username, "-c", " ".join(quoted_args)])
    # TODO parse sys.argv to append addons path if command is odoo
    # we can introspect /addons/* and env ODOO_BASE_PATH to compute
    # the right addons path to pass to the process
    print("Starting main command", sys.argv)

    return pipe(sys.argv[1:])


def call_sudo_entrypoint():
    ret = pipe(["sudo", "-H", "/sudo-entrypoint.py"])
    return ret


def install_python_dependencies():
    """
    Install all the requirements.txt file found
    """
    # TODO
    # https://pypi.org/project/requirements-parser/
    # to parse all the requirements file to parse all the possible specs
    # then append the specs to the loaded requirements and dump the requirements.txt
    # file in /var/lib/odoo/requirements.txt and then install this only file
    # instead of calling multiple time pip
    requirement_files = glob.glob("/addons/**/requirements.txt")
    requirement_files.sort()

    print("Installing python requirements found in:")
    print("    \n".join(requirement_files))

    for requirements in requirement_files:
        print("Installing python packages from %s" % requirements)
        flush_streams()
        # pip.main(['install', '-r', requirements])
        pipe(["pip", "install", "-r", requirements])
        print("Done")
        flush_streams()

    print("Installing python requirements complete\n")


def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def install_master_password(config_path):
    # Secure an odoo instance with a default master password
    # if required we can update the master password but at least
    # odoo doesn't get exposed by default without master passwords
    print("Installing master password in ODOORC")

    ctx = CryptContext(['pbkdf2_sha512', 'plaintext'], deprecated=['plaintext'])
    config = ConfigParser()
    config.read(config_path)

    master_password_secret = "/run/secrets/master_password"
    if path.exists(master_password_secret):
        with open(master_password_secret, "r") as mp:
            master_password = mp.read().strip()
    elif os.environ.get('MASTER_PASSWORD'):
        master_password = os.environ.get('MASTER_PASSWORD')
    else:
        master_password = randomString(64)

        if os.environ.get('DEPLOYMENT_AREA') == 'undefined':
            print(
                "Use this randomly generated master password"
                " to manage the database"
            )
            print("    %s" % master_password)


    # Check that we don't have plaintext and encrypt it
    # This allow us to quickly setup servers without having to hash ourselves first
    # for security reason, you should always hash the password first and not expect
    # the image to do it correctly
    # but older version of odoo do not support encryption so only encrypt
    # older version of odoo...
    if (
        float(os.environ.get('ODOO_VERSION')) > 10 and
        ctx.identify(master_password) == 'plaintext'
    ):
        master_password = ctx.encrypt(master_password)

    config.set('options', 'admin_passwd', master_password)

    with open(config_path, 'w') as out:
        config.write(out)

    print("Installing master password completed")

def setup_environ(config_path):
    print("Configuring environment variables for postgresql")
    config = ConfigParser()
    config.read(config_path)

    def get_option(config, section, name):
        try:
            return config.get(section, name)
        except NoOptionError:
            return None

    def check_config(config_name, config_small):
        """
        Check if config is in odoo_rc or command line
        """
        value = None

        if get_option(config, 'options', config_name):
            value = get_option(config, 'options', config_name)

        if not value and '--%s' % config_name in sys.argv:
            idx = sys.argv.index('--%s' % config_name)
            value = sys.argv[idx+1] if idx < len(sys.argv) else None

        if not value and config_small and '-%s' % config_small in sys.argv:
            idx = sys.argv.index('-%s' % config_small)
            value = sys.argv[idx+1] if idx < len(sys.argv) else None

        return value

    variables = [
        ('PGUSER', 'db_user', 'r'),
        ('PGHOST', 'db_host', None),
        ('PGPORT', 'db_port', None),
        ('PGDATABASE', 'database', 'd')
    ]

    # Setup basic PG env variables to simplify managements
    # combined with secret pg pass we can use psql directly
    for pg_val, odoo_val, small_arg in variables:
        value = check_config(odoo_val, small_arg)
        if value:
            os.environ[pg_val] = value

    print("Configuring environment variables done")

def main():
    # Install apt package first then python packages
    ret = call_sudo_entrypoint()

    if ret not in [0, None]:
        sys.exit(ret)

    # Install python packages with pip in user home
    install_python_dependencies()
    install_master_password(os.environ.get('ODOO_RC'))
    setup_environ(os.environ.get('ODOO_RC'))

    return start()

def flush_streams():
    sys.stdout.flush()
    sys.stderr.flush()

try:
    code = main()
    flush_streams()
    sys.exit(code)
except Exception as exc:
    print(exc)
    import traceback
    traceback.print_exc()
    flush_streams()
    sys.exit(1)
except KeyboardInterrupt as exc:
    print(exc)
    import traceback
    traceback.print_exc()
    flush_streams()
    sys.exit(1)
